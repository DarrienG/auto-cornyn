from selenium import webdriver
from selenium.webdriver.chromium.webdriver import ChromiumDriver


def chrome_driver() -> ChromiumDriver:
    options = webdriver.ChromeOptions()
    options.add_argument("--disable-blink-features")
    options.add_argument("--disable-blink-features=AutomationControlled")

    driver = webdriver.Chrome(options=options)
    # Can run fullscreen if desired, but makes it tough to multitask
    # driver.maximize_window()
    return driver
