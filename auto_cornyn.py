#!/usr/bin/env python3
import argparse
import random
import sys
from typing import Optional

import colorama
from colorama import Fore, Style

from automator.boozman import auto_boozman
from automator.braun import auto_braun
from automator.capito import auto_capito
from automator.coal_guy import auto_coal_guy
from automator.corn import auto_corn
from automator.ernst import auto_ernst
from automator.grass import auto_grass
from automator.sinema import auto_sinema
from automator.tillis import auto_tillis
from driver import initializer
from sheet.iterator import spreadsheeterator, mark_complete, SheetFormat
from states.letter_codes import StateCode, print_state

# Yeah - global state is bad, whatever
# There's a better way to do this, but I'm not paid to do this
SINGLE_STATE_SKIP = False

DEFAULT_LIMIT = 100
JITTER_LOWER_BOUND = 9
JITTER_UPPER_BOUND = 28


def should_take_state(
    current_state: Optional[str],
    one_state_option: Optional[str],
    section_supported_state: StateCode,
) -> bool:
    section_value = section_supported_state.value
    result = False
    if one_state_option:
        if current_state == section_value:
            global SINGLE_STATE_SKIP
            SINGLE_STATE_SKIP = True
        result = current_state == section_value and current_state == one_state_option
    else:
        result = current_state == section_value

    if result:
        print_state(section_supported_state)
    return result


def get_limit_with_jitter(limit: int) -> int:
    if limit == DEFAULT_LIMIT:
        is_negative = -1 if bool(random.getrandbits(1)) else 1
        return limit + (
            random.randint(JITTER_LOWER_BOUND, JITTER_UPPER_BOUND) * is_negative
        )
    else:
        return limit


def main():
    colorama.init()
    parser = argparse.ArgumentParser(description="Automatic senator submissions")
    parser.add_argument(
        "--limit",
        "-l",
        default=DEFAULT_LIMIT,
        help=f"Number of entries to run at a time. By default it will run {DEFAULT_LIMIT} with a jitter of + or - {JITTER_LOWER_BOUND} to {JITTER_UPPER_BOUND}",
    )
    parser.add_argument(
        "--sheet", "-s", default="19BXw3VliGpRjXWybgXMV-NNV-u1-wRNh_5fojH1wtO8"
    )
    parser.add_argument(
        "--format",
        "-f",
        default="dreamers1",
        help="dreamers1, dreamers2, parents1, parents2",
    )
    parser.add_argument(
        "--one-state",
        "-o",
        required=False,
        default=None,
        help="Only run on one single state. Use two letter abbreviation of state name",
    )
    args = parser.parse_args()
    limit = args.limit
    sheet = args.sheet
    # Only one format nowadays yeehaw
    sheet_format = SheetFormat.NEW_JULY_2024
    one_state = args.one_state
    amt_complete = 0
    global SINGLE_STATE_SKIP
    for value in spreadsheeterator(sheet, sheet_format):
        if not value.valid_user_data():
            print(Fore.RED + "[!E]" + Style.RESET_ALL, end="", flush=True)
            continue
        if amt_complete == limit:
            print(Fore.CYAN + "[!L]" + Style.RESET_ALL, end="", flush=True)
            return
        elif value.complete:
            print(Fore.GREEN + "[✓]" + Style.RESET_ALL, end="", flush=True)
            continue
        elif should_take_state(value.state, one_state, StateCode.TEXAS):
            amt_complete += 1
            with initializer.chrome_driver() as driver:
                auto_corn(driver, value.to_user_data())
                mark_complete(value.current_row - 1, sheet, sheet_format)
        elif should_take_state(value.state, one_state, StateCode.ARIZONA):
            amt_complete += 1
            with initializer.chrome_driver() as driver:
                auto_sinema(driver, value.to_user_data())
                mark_complete(value.current_row - 1, sheet, sheet_format)
        elif should_take_state(value.state, one_state, StateCode.IOWA):
            amt_complete += 1
            with initializer.chrome_driver() as driver:
                auto_grass(driver, value.to_user_data())
                auto_ernst(driver, value.to_user_data())
                mark_complete(value.current_row - 1, sheet, sheet_format)
        elif should_take_state(value.state, one_state, StateCode.NORTH_CAROLINA):
            amt_complete += 1
            with initializer.chrome_driver() as driver:
                auto_tillis(driver, value.to_user_data())
                mark_complete(value.current_row - 1, sheet, sheet_format)
        elif should_take_state(value.state, one_state, StateCode.ARKANSAS):
            amt_complete += 1
            with initializer.chrome_driver() as driver:
                auto_boozman(driver, value.to_user_data())
                # Currently failing with:
                # Message: element click intercepted: Element is not clickable at point (1133, 826)
                # auto_cotton(driver, value.to_user_data())
                mark_complete(value.current_row - 1, sheet, sheet_format)
        elif should_take_state(value.state, one_state, StateCode.WEST_VIRGINIA):
            amt_complete += 1
            with initializer.chrome_driver() as driver:
                auto_capito(driver, value.to_user_data())
                auto_coal_guy(driver, value.to_user_data())
                mark_complete(value.current_row - 1, sheet, sheet_format)
        elif should_take_state(value.state, one_state, StateCode.INDIANA):
            amt_complete += 1
            with initializer.chrome_driver() as driver:
                # Need to properly handle the horrible email popup thing
                # auto_young(driver, value.to_user_data())
                auto_braun(driver, value.to_user_data())
                mark_complete(value.current_row - 1, sheet, sheet_format)
        else:
            if SINGLE_STATE_SKIP:
                print(Fore.LIGHTWHITE_EX + "[!O]" + Style.RESET_ALL, end="", flush=True)
                SINGLE_STATE_SKIP = False
            else:
                print(".", end="", flush=True)


if __name__ == "__main__":
    main()
