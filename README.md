# Auto-Cornyn

If you know, you know ;D

## Running instructions

Make sure you have chromedriver in your PATH and Chrome installed.

If you don't have it, open Chrome and click About to get your Chrome version.
Then download the corresponding chromedriver binary and add it to your PATH.

Downloads can be found here: https://chromedriver.chromium.org/downloads

If you don't know what that means, you can just do this if you're on Mac or
Linux:

```bash
export PATH="$PATH:~/Downloads"
```

Grab yourself a Google API token from our API console and place it in a file in
the root called `credentials.json`.

If you want extra number support, grab `fax_numbers.json` and place it in the
root of the directory too.

Then in the same shell, you can run the auto_cornyn script like so:

```bash
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt

# Auto run through spreadsheet
python auto_cornyn.py

# Customizable runs too, all flags can be used at once

# Only run through 25 entries, default is 100 with jitter of + or - 9-28
# No jitter is added if default value of 100 is not chosen
python auto_cornyn.py --limit 25

# Specify a specific google sheet with schema, default is
# dreamer format with 1S2nyY6McCcfPwg2opsSBIB84UQ0kmeBeq1uY6FW6IPc
python auto_cornyn.py -s 1DRQBfABJNwDTZGqpV-dkXloWcsfzNJo1N2fu1wx57H4 --format parents1

# Can be run with both parents and dreamers type sheets. By default it runs with a
# parent type sheet, but can support both:

python auto_cornyn.py --format parents1
python auto_cornyn.py --format dreamers2
```

Note that parents/dreamers sheets must be in 1 of 4 formats and in order. The
script expects the sheet will be in the order:
* Sheet6 (or any value)
* Dreamers1
* Dreamers2
* Parents1
* Parents2
* Anything else

On successful run, the C column will be overwritten with:
* TRUE


## Understanding the log format

While running through a spreadsheet, you may notice progress indicators. Here's
what each one means:
* `.` => looked at a cell, state was not relevant
* `[AZ]` => Arizona, needs to be filled out
* `[IA]` => Iowa, needs to be filled out
* `[NC]` => North Carolina, needs to be filled out
* `[TX]` => Texas, needs to be filled out
* `[✓]` => Found row already complete, skipping
* `[!O]` => Supported state, but skipped because single state was set
* `[!L]` => Hit user given limit. Stopping execution
* `[!X]` => Malformed row (does not conform to parent or dreamer schema) skipping
* `[!E]` => Well formed row, but malformed user data (e.g. invalid email) skipping
* `[!s10]` => Got rate limit exception, pausing execution for 10 seconds
* `[CX!!]` => Spreadsheet completed
