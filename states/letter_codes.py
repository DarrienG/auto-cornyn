from enum import Enum


class StateCode(Enum):
    ARKANSAS = "AR"
    ARIZONA = "AZ"
    IOWA = "IA"
    NORTH_CAROLINA = "NC"
    TEXAS = "TX"
    WEST_VIRGINIA = "WV"
    INDIANA = "IN"


def print_state(state_code: StateCode):
    print(f"[{state_code.value}]", end="", flush=True)
