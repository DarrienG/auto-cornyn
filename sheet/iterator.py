import json
import os
import random
from dataclasses import dataclass
from enum import Enum
from time import sleep
from typing import Optional, Generator

import gspread
import zipcodes
from colorama import Style, Fore
from gspread.exceptions import APIError

from base_types.user_data import UserData

# If modifying these scopes, delete the file token.json.
SCOPES = ["https://www.googleapis.com/auth/spreadsheets"]


def get_random_last_name() -> str:
    random_last_names = ["Kahn", "Jain", "Sharma", "Smith", "Banerjee", "Chandra", "Rai", "Viswanathan"]
    return random.choice(random_last_names)


class SheetFormat(Enum):
    NEW_JULY_2024 = 0

    def get_idx(self) -> int:
        """
        Converts enum to sheet index value number.
        Technically we could just call value, but this is a little clearer
        :return: index value
        """
        return self.value


def get_numbers() -> list[str]:
    if not os.path.exists("fax_numbers.json"):
        print("No extra numbers loaded, no fax_numbers.json, using [9789302385] only")
        return ["9789302385"]

    with open('fax_numbers.json') as json_file:
        print("Extra numbers loaded from fax_numbers.json")
        data = json.load(json_file)
        return data["IA"] + data["IN"]


NUMBERS = get_numbers()


@dataclass
class RowData:
    created_date: str
    name: str
    address: str
    email: str
    zipcode: str
    complete: bool
    current_row: int
    full_message: str
    state: str
    _true_zipcode: Optional[str] = None
    _city: Optional[str] = None

    def __post_init__(self):
        if len(self.zipcode) == 4:
            self._true_zipcode = f"0{self.zipcode}"
        elif len(self.zipcode) == 5:
            self._true_zipcode = f"{self.zipcode}"
        elif len(self.zipcode) == 10 and self.zipcode[5] == "-":
            self._true_zipcode = f"{self.zipcode}"
        else:
            # partial init state is ok as long as the user
            # later checks valid_user_data
            return

        zipcode_info = zipcodes.matching(self._true_zipcode)[0]
        if not self.state:
            self.state = zipcode_info["state"]

        self._city = zipcode_info["city"]

    def to_user_data(self) -> UserData:
        names = self.name.split(" ")
        first_name = names[0]
        # Randomize this later
        last_name = get_random_last_name() if len(names) < 2 else " ".join(names[1:])
        return UserData(
            first_name=first_name,
            last_name=last_name,
            email=self.email,
            phone_number=random.choice(NUMBERS),
            address=self.address,
            city=str(self._city),
            state_code=self.state,
            zip_code=str(self._true_zipcode),
            full_message=self.full_message
        )

    def valid_user_data(self) -> bool:
        return "@" in self.email and self._true_zipcode


def spreadsheeterator(sheet_id: str, sheet_format: SheetFormat) -> Generator[RowData, None, None]:
    """
    Gets an iterator to current spreadsheet and returns values until there is nothing left
    :return:
    """

    gc = gspread.service_account(filename="credentials.json")
    sheet = gc.open_by_key(sheet_id).get_worksheet(sheet_format.get_idx())
    current_row_num = 2

    for idx, current_value in enumerate(sheet.get_values()):
        # The first one is the row data, this is useless to us
        if idx == 0:
            continue
        try:
            print(current_row_num + idx + 1)
            if sheet_format == SheetFormat.NEW_JULY_2024:
                #if current_value[9] == "czjohnson@comcast.net":
                #    print(current_value)
                #    raise Exception("enough")
                yield RowData(
                    created_date=current_value[4],
                    name=current_value[6],
                    address=current_value[8],
                    email=current_value[9],
                    zipcode=current_value[10],
                    state=current_value[11],
                    full_message=current_value[13],
                    complete=True if current_value[16].lower() == "yes" else False,
                    current_row=current_row_num + idx + 1,
                )
            else:
                raise Exception(
                    "Got invalid sheet type. This should be impossible. "
                    "Or someone added a new sheet type and didn't add an implementation.")
        except IndexError as e:
            # Invalid row, nothing we can do here
            print(Fore.RED + "[!X]" + Style.RESET_ALL, end="", flush=True)
        except:
            # Probably got rate limiting error, but if this happens a lot, double check here...
            print(Fore.MAGENTA + "[!S10]" + Style.RESET_ALL, end="", flush=True)
            sleep(10)

    print(Fore.YELLOW + "[CX!!]" + Style.RESET_ALL)


def mark_complete(row: int, sheet: str, sheet_format: SheetFormat):
    raised_exception = True

    while raised_exception:
        try:
            _mark_complete_unsafe(row, sheet, sheet_format)
            raised_exception = False
        except APIError:
            raised_exception = True
            print(Fore.MAGENTA + "[!S10]" + Style.RESET_ALL, end="", flush=True)
            sleep(10)


def _mark_complete_unsafe(row: int, sheet: str, sheet_format: SheetFormat):
    gc = gspread.service_account(filename="credentials.json")
    wks = gc.open_by_key(sheet).get_worksheet(sheet_format.get_idx())
    wks.update(f"{'R'}{row - 1}", "Yes")


def value_to_bool(current_value: str) -> bool:
    return current_value == "DONE"
