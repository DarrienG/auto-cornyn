from dataclasses import dataclass
from typing import Optional


@dataclass
class UserData:
    first_name: str
    last_name: str
    email: str
    phone_number: str
    address: str
    city: str
    state_code: str
    zip_code: str
    full_message: str
