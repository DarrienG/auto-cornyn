from time import sleep

from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from automator.submission import item_on_page, submit_text
from base_types.user_data import UserData
from brief_message import writer


def auto_tillis(driver: ChromiumDriver, user_data: UserData):
    driver.get("https://www.tillis.senate.gov/email-me")

    select_title = Select(driver.find_element(By.ID, "field_700BE99B-C12A-4836-BD02-D87FCD7ADECD"))
    select_title.select_by_value("Mr.")  # if we have this data we should set it

    select_title = Select(driver.find_element(By.ID, "field_957A77F6-59C6-4500-8A3B-9AE76484F733"))
    select_title.select_by_value("IMM")  # if we have this data we should set it

    form_inputs = [
        ("field_279EFB03-F78D-4924-8599-F04AEB99A7A6", user_data.full_message),
        ("field_554EB441-1332-47D8-B817-87A030531FCC", user_data.first_name),
        ("field_77689247-ECBE-4E25-97D6-62F8A979E226", user_data.last_name),
        ("field_5C0C729A-D702-46F1-9821-7DD7E565E136", user_data.email),
        ("field_47F809FC-8DE7-4768-95BE-6B94DCC7C8C8", user_data.address),
        ("field_965E8165-21F8-49A4-AAD0-E72A361EC2DB", user_data.city),
        ("field_F35EA2FD-C92A-4FFC-9CE0-637B8AEECB54", user_data.zip_code),
        ("field_64322B9D-7F15-47DA-B656-9843D7D04866", user_data.phone_number),
        ("field_E2D2548D-F3B7-48F7-9BD0-E29F4ED82A43",
         "Please support the bipartisan America's children act")
    ]

    for form_input in form_inputs:
        submit_text(driver, *form_input)

    # State is autofilled to the correct one, we don't need to fill it

    while item_on_page(driver, By.CLASS_NAME, "btn-primary"):
        sleep(0.5)
