from time import sleep

from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from automator.submission import submit_text, item_on_page
from base_types.user_data import UserData
from brief_message import writer


def auto_ernst(driver: ChromiumDriver, user_data: UserData):
    driver.get("https://www.ernst.senate.gov/contact/email-joni")

    while not item_on_page(driver, By.ID, "title"):
        # Loads with JS, so let's hang tight while it takes its sweet time
        sleep(1)
    select_title = Select(driver.find_element(By.ID, "title"))
    select_title.select_by_value("Mr.")  # if we have this data we should set it

    select = Select(driver.find_element("id", "topic"))
    select.select_by_value("IMM")

    form_inputs = [
        ("input-5C18D021-0988-F2BB-1F6A-3665652A79E9",
         writer.special_ernst_message(user_data.first_name, user_data.last_name, user_data.city)),
        ("input-5C189E34-95D2-102E-CED2-A264D066F6C3", "Continued support for Documented Dreamers"),
        # put customized joni subject here
        ("input-5C0E54FD-9A0F-A898-ABE9-77C0ED64809E", user_data.first_name),
        ("input-5C29EA3E-028C-010F-7E0E-A265D5A6DE43", user_data.last_name),
        ("email", user_data.email),
        ("input-5C1768B7-F54F-4C52-0BD9-FD81BC59D6B3", user_data.phone_number),
        ("input-5C181C7B-F049-21D2-D2C9-B2FAEBC4C296", user_data.address),
        ("input-5C181CE0-D1B4-E41B-8FC0-A0226E16AF06", user_data.city),
        ("input-5C181D46-F77C-5CE6-3BA3-0C38F8DBDAE6", user_data.zip_code),
    ]

    for form_input in form_inputs:
        submit_text(driver, *form_input)

    # state code is autofilled, no need to have

    driver.find_element(By.CLASS_NAME, "btn").click()

    while item_on_page(driver, By.CLASS_NAME, "btn"):
        sleep(0.5)

    sleep(2)
