import random
from time import sleep

from selenium.common import NoSuchElementException
from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.common.by import By


def submit_text(driver: ChromiumDriver, field_id: str, input_text: str):
    input_field = driver.find_element(By.ID, field_id)
    input_field.send_keys(input_text)
    sleep(random.uniform(0.3, 1))


def item_on_page(driver: ChromiumDriver, by: str, item: str) -> bool:
    try:
        driver.find_element(by, item)
        return True
    except NoSuchElementException:
        return False
