from time import sleep

from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from automator.submission import submit_text, item_on_page
from base_types.user_data import UserData
from brief_message import writer


def auto_braun(driver: ChromiumDriver, user_data: UserData):
    driver.get("https://www.braun.senate.gov/contact-mike")

    select = Select(driver.find_element(By.ID, "input_2_39"))

    select.select_by_value("Mr.")

    select = Select(driver.find_element(By.ID, "input_2_17"))
    select.select_by_value("Immigration")

    form_inputs = [
        ("input_2_11", writer.subject()),
        (
            "input_2_12",
            user_data.full_message,
        ),
        ("input_2_2", user_data.first_name),
        ("input_2_3", user_data.last_name),
        ("input_2_9", user_data.email),
        ("input_2_9_2", user_data.email),
        ("input_2_6_1", user_data.address),
        ("input_2_6_3", user_data.city),
        ("input_2_6_5", user_data.zip_code),
        ("input_2_7", user_data.phone_number),
    ]

    for form_input in form_inputs:
        submit_text(driver, *form_input)

    driver.find_element(By.ID, "gform_submit_button_2").click()

    while item_on_page(driver, By.ID, "input_2_12"):
        sleep(0.5)

    sleep(2)
