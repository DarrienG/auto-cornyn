from time import sleep

from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from automator.submission import item_on_page, submit_text
from base_types.user_data import UserData
from brief_message import writer


def auto_grass(driver: ChromiumDriver, user_data: UserData):
    driver.get("https://www.grassley.senate.gov/contact/questions-and-comments")

    while not item_on_page(driver, By.ID, "issues"):
        # I'm tired of all of this JS
        sleep(1)

    select = Select(driver.find_element(By.ID, "issues"))
    select.select_by_value("Immigration")

    form_inputs = [
        ("input-BADE811D-5056-A066-600C-4341F0A391DA", user_data.full_message),
        ("input-BADE8285-5056-A066-6057-C322607D9298", user_data.first_name),
        ("input-BADE7F00-5056-A066-609F-EA07AE2BF91B", user_data.last_name),
        ("input-BADE8249-5056-A066-6085-94AFC521D3DD", user_data.email),
        ("input-BADE815A-5056-A066-6029-7AE648141C0E", user_data.email),  # verify email box
        # ("input-BADE833B-5056-A066-6069-DE27AD45B36B", user_data.phone_number), Optional - so we skip
        ("input-BADE8377-5056-A066-601E-439FF206EE9C", user_data.address),
        ("input-BADE8474-5056-A066-6074-6537A53F1741", user_data.city),
        ("input-BADE7F3C-5056-A066-60C0-B76B321104A2", user_data.zip_code),
        ("input-BADE8430-5056-A066-60F2-B5FA10EC77B8", "Support Bipartisan Amendment #923 to NDAA")
    ]

    for form_input in form_inputs:
        submit_text(driver, *form_input)

    # State is autofilled to the correct one, we don't need to fill it

    driver.find_element(By.CLASS_NAME, "btn").click()

    while item_on_page(driver, By.CLASS_NAME, "btn"):
        sleep(0.5)

    sleep(2)
