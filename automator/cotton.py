from time import sleep

from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from automator.submission import submit_text, item_on_page
from base_types.user_data import UserData
from brief_message import writer


def auto_cotton(driver: ChromiumDriver, user_data: UserData):
    driver.get("https://www.cotton.senate.gov/contact/contact-tom")

    select = Select(
        driver.find_element(By.ID, "input-B03E04DB-4040-F985-52CD-18F332336F99")
    )
    select.select_by_value("Family")

    form_inputs = [
        (
            "input-B03E05B4-4040-F985-52CD-8DF0BC10EEE2",
            user_data.full_message,
        ),
        ("input-BC4D4A76-EAA0-9925-E36C-A9DD5D8581F2", writer.subject()),
        ("input-B03E05C4-4040-F985-52CD-9CA44623DE5E", user_data.first_name),
        ("input-B03E04BD-4040-F985-52CD-48E090E769DA", user_data.last_name),
        ("email", user_data.email),
        ("input-B03E046E-4040-F985-52CD-A2F10450D58D", user_data.phone_number),
        ("input-B03E05F1-4040-F985-52CD-A580E72A7D41", user_data.address),
        ("input-B03E0595-4040-F985-52CD-C572364023FE", user_data.city),
        ("input-B03E04CC-4040-F985-52CD-4DDB58DA474A", user_data.zip_code),
    ]

    for form_input in form_inputs:
        submit_text(driver, *form_input)

    driver.find_element(By.CLASS_NAME, "btn").click()

    while item_on_page(driver, By.ID, "input-B03E04CC-4040-F985-52CD-4DDB58DA474A"):
        sleep(0.5)

    sleep(2)
