from time import sleep

from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from automator.submission import submit_text, item_on_page
from base_types.user_data import UserData
from brief_message import writer
from js.click import js_click


def auto_coal_guy(driver: ChromiumDriver, user_data: UserData):
    driver.get("https://www.manchin.senate.gov/contact-joe/email-joe")

    # Nice try with the JS Mr. Fossil Fuel, but you can't trick me
    js_click(driver, By.CLASS_NAME, "opinion")

    while not item_on_page(driver, By.ID, "input-B03E05C4-4040-F985-52CD-9CA44623DE5E"):
        sleep(0.5)

    select = Select(
        driver.find_element("id", "input-B03E0527-4040-F985-52CD-C0DD6BEF325F")
    )
    select.select_by_value("Mr.")

    select = Select(
        driver.find_element("id", "input-B03E04DB-4040-F985-52CD-18F332336F99")
    )
    select.select_by_value("IMM")

    form_inputs = [
        ("input-CD35B39E-05AB-A9CD-A328-4AA2A35FE971", writer.subject()),
        (
            "input-B03E05B4-4040-F985-52CD-8DF0BC10EEE2",
            user_data.full_message,
        ),
        ("input-B03E05C4-4040-F985-52CD-9CA44623DE5E", user_data.first_name),
        ("input-B03E04BD-4040-F985-52CD-48E090E769DA", user_data.last_name),
        ("email", user_data.email),
        ("input-B03E05A4-4040-F985-52CD-730C08912F4F", user_data.email),  # verify email
        ("input-B03E046E-4040-F985-52CD-A2F10450D58D", user_data.phone_number),
        ("input-B03E05F1-4040-F985-52CD-A580E72A7D41", user_data.address),
        ("input-B03E0595-4040-F985-52CD-C572364023FE", user_data.city),
        ("input-B03E04CC-4040-F985-52CD-4DDB58DA474A", user_data.zip_code),
    ]

    for form_input in form_inputs:
        submit_text(driver, *form_input)

    driver.find_element(By.CLASS_NAME, "btn").click()

    while item_on_page(driver, By.ID, "input-B03E05A4-4040-F985-52CD-730C08912F4F"):
        sleep(0.5)

    sleep(2)
