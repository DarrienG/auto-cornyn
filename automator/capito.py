from time import sleep

from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from automator.submission import submit_text, item_on_page
from base_types.user_data import UserData
from brief_message import writer
from js.click import js_click


def auto_capito(driver: ChromiumDriver, user_data: UserData):
    driver.get("https://www.capito.senate.gov/contact/contact-my-office")

    # Love JS loading the main page
    while not item_on_page(driver, By.ID, "input-AB59F08A-4040-F985-52CD-7633AA11C65E"):
        sleep(0.5)

    select = Select(
        driver.find_element("id", "input-AB59F08A-4040-F985-52CD-7633AA11C65E")
    )
    select.select_by_value("Mr.")

    select = Select(
        driver.find_element("id", "input-AB8156A0-4040-F985-52CD-49CE65770EFC")
    )
    select.select_by_value("Immigration")

    form_inputs = [
        ("input-936910F5-4040-F985-52CD-A6FC381A979D", writer.subject()),
        (
            "input-ADE64E73-4040-F985-52CD-A3AB6BCD2C51",
            user_data.full_message,
        ),
        ("input-AB59F0AE-4040-F985-52CD-AA63153C7925", user_data.first_name),
        ("input-AB59F0ED-4040-F985-52CD-265D9CF13F5E", user_data.last_name),
        ("email", user_data.email),
        ("input-AB721CE8-4040-F985-52CD-9E550E8A76AA", user_data.email),  # verify email
        ("input-AB67BC2F-4040-F985-52CD-007A86DA4D47", user_data.phone_number),
        ("input-AB5B6829-4040-F985-52CD-BF81DCC1F57D", user_data.address),
        ("input-AB5B6875-4040-F985-52CD-909541CB4B6C", user_data.city),
        ("input-AB5B68B4-4040-F985-52CD-269A4338D057", user_data.zip_code),
    ]

    for form_input in form_inputs:
        submit_text(driver, *form_input)

    js_click(driver, By.CLASS_NAME, "btn", True)

    while item_on_page(driver, By.ID, "input-AB5B68B4-4040-F985-52CD-269A4338D057"):
        sleep(0.5)

    sleep(2)
