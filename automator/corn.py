from time import sleep

from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from automator.submission import submit_text, item_on_page
from base_types.user_data import UserData
from brief_message import writer


def auto_corn(driver: ChromiumDriver, user_data: UserData):
    driver.get("https://www.cornyn.senate.gov/share-opinion/")

    select = Select(driver.find_element(By.ID, "form-field-field_10b4eab"))
    select.select_by_value("Immigration")

    form_inputs = [
        (
            "form-field-field_e1bcf38",
            user_data.full_message,
        ),
        ("form-field-name", user_data.first_name),
        ("form-field-field_0b2855e", user_data.last_name),
        ("form-field-email", user_data.email),
        ("form-field-field_ecfbd0b", user_data.phone_number),
        ("form-field-field_4b6fd40", user_data.address),
        ("form-field-field_3c0335e", user_data.city),
        ("form-field-field_bff6d4a", user_data.zip_code),
    ]

    for form_input in form_inputs:
        submit_text(driver, *form_input)

    driver.find_element(By.CLASS_NAME, "elementor-button-text").click()

    while item_on_page(driver, By.ID, "form-field-field_bff6d4a"):
        sleep(0.5)

    sleep(2)
