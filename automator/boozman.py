from time import sleep

from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.common.by import By

from automator.submission import submit_text, item_on_page
from base_types.user_data import UserData
from brief_message import writer


def auto_boozman(driver: ChromiumDriver, user_data: UserData):
    driver.get("https://www.boozman.senate.gov/public/index.cfm/e-mail-me")

    form_inputs = [
        (
            "field_48423449-A5CB-4B5E-8AF8-6941AB24E7BC",
            user_data.full_message,
        ),
        ("field_AB4DFB2A-39F2-4EA3-AD48-98FF4203C4EB", user_data.first_name),
        ("field_82A4F7B4-E157-4AD1-8A1E-3186B3660C2E", user_data.last_name),
        ("field_1A65E495-5A0F-444D-B7D5-4BC56EB418BE", user_data.email),
        ("field_D19C2B48-DE96-4320-87BE-604F298E8497", user_data.phone_number),
        ("field_17ECEDEA-027F-4974-877D-86168D5A1AC6", user_data.address),
        ("field_0AFBF97C-2198-48CC-9A1F-24102686CEAD", user_data.city),
        ("field_484880FE-DA5B-4994-B590-B6CE1B323F1E", user_data.zip_code),
    ]

    for form_input in form_inputs:
        submit_text(driver, *form_input)

    driver.find_element(By.CLASS_NAME, "btn-primary").click()

    while item_on_page(driver, By.ID, "field_484880FE-DA5B-4994-B590-B6CE1B323F1E"):
        sleep(0.5)

    sleep(2)
