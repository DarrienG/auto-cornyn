from time import sleep

from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from automator.submission import item_on_page, submit_text
from base_types.user_data import UserData
from brief_message import writer


def auto_sinema(driver: ChromiumDriver, user_data: UserData):
    driver.get("https://www.sinema.senate.gov/contact-kyrsten")

    select = Select(driver.find_element("id", "edit-issue"))
    select.select_by_value("Immigration")
    select_title = Select(driver.find_element("name", "prefix"))
    select_title.select_by_value("Mr.")  # if we have this data we should set it

    form_inputs = [
        ("edit-message",
         writer.special_sinema_message(user_data.first_name, user_data.last_name)),
        ("edit-first-name", user_data.first_name),
        ("edit-last-name", user_data.last_name),
        ("edit-email", user_data.email),
        ("edit-address-1", user_data.address),
        ("edit-city", user_data.city),
        ("edit-zip", user_data.zip_code),
        ("edit-phone", user_data.phone_number),
        ("edit-subject", "Please include children of legal immigrants in bipartisan immigration bill")
    ]

    for form_input in form_inputs:
        submit_text(driver, *form_input)

    driver.find_element(By.ID, "edit-sign-up-for-updates-no").click()

    # State is autofilled to the correct one, we don't need to fill it

    driver.find_element(By.ID, "edit-actions-submit").click()

    while not item_on_page(driver, By.CLASS_NAME, "webform-confirmation__back"):
        sleep(0.5)

    sleep(2)
