#!/usr/bin/env python3

from typing import Optional


def subject() -> str:
    return "Please support S. 1667/HR. 3442"


def immigration_message(
    first_name: str,
    last_name: Optional[str] = None,
    personal_statement: Optional[str] = None,
) -> str:
    return f"""
My name is {first_name} and I am writing to you as a constituent who would benefit from your support as a cosponsor of the bipartisan America’s Children Act, HR. 3442 and S. 1667, and ensuring that bipartisan age-out protections are included in any end-of-the-year bill.

Maintaining our nation’s competitive advantage is essential to national security. Currently, we are forcing American-raised legal immigrant children, who have been educated here, to leave the country due to a flaw in our system. This does not make sense. Over 200,000 children of long-term visa holders who have maintained legal status living in the United States and have received advanced American degrees are unable to stay and contribute to the only country they’ve known. This issue has broad bipartisan support.

Allowing children of long-term visa holders, who are essentially American, to stay, will help our communities. Not fixing this is only hurting our country and the economy as the majority of these individuals are STEM graduates.

I sincerely urge you to cosponsor S. 1667/HR. 3442, and ensure the inclusion of age-out protections for young immigrants in any end of the year bill.

Thank you,

{first_name} {last_name if last_name else ""}
"""


def special_ernst_message(first_name: str, last_name: Optional[str], city: str):
    return f"""
Dear Senator Ernst,

My name is {first_name}. I am thankful for Senator Ernst being a cosponsor of this bill during the last Congress (S. 2753).

I am writing to you as a constituent who would benefit from your support as a cosponsor of the bipartisan America’s Children Act, HR. 3442 and S. 1667, and ensuring that bipartisan age-out protections are included in any end-of-the-year bill.

Maintaining our nation’s competitive advantage is essential to national security. Currently, we are forcing American-raised legal immigrant children, who have been educated here, to leave the country due to a flaw in our system. This does not make sense. Over 200,000 children of long-term visa holders who have maintained legal status living in the United States and have received advanced American degrees are unable to stay and contribute to the only country they’ve known. This issue has broad bipartisan support.

Allowing children of long-term visa holders, who are essentially American, to stay, will help our communities. Not fixing this is only hurting our country and the economy as the majority of these individuals are STEM graduates.

I'd like to thank you for being a co-sponsor of S.2753. 
I sincerely urge you to cosponsor S.1667, and ensure the inclusion of age-out protections for young immigrants in any end of the year bill.

Thank you,
{first_name} {last_name if last_name else ""}
"""


def special_sinema_message(first_name: str, last_name: Optional[str]):
    return f"""
Dear Senator Sinema,

My name is {first_name}. I am thankful for Senator Sinema being a cosponsor of this bill during the last Congress (S. 2753).

I am writing to you as a constituent who would benefit from your support as a cosponsor of the bipartisan America’s Children Act, HR. 3442 and S. 1667, and ensuring that bipartisan age-out protections are included in any end-of-the-year bill.

Maintaining our nation’s competitive advantage is essential to national security. Currently, we are forcing American-raised legal immigrant children, who have been educated here, to leave the country due to a flaw in our system. This does not make sense. Over 200,000 children of long-term visa holders who have maintained legal status living in the United States and have received advanced American degrees are unable to stay and contribute to the only country they’ve known. This issue has broad bipartisan support.

Allowing children of long-term visa holders, who are essentially American, to stay, will help our communities. Not fixing this is only hurting our country and the economy as the majority of these individuals are STEM graduates.

I'd like to thank you for being a co-sponsor of S.2753. 
I sincerely urge you to cosponsor S.1667, and ensure the inclusion of age-out protections for young immigrants in any end of the year bill.

Thank you,
{first_name} {last_name if last_name else ""}
"""
