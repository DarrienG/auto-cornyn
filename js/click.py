import time
from typing import Optional

from selenium.webdriver import ActionChains, Keys
from selenium.webdriver.chromium.webdriver import ChromiumDriver
from selenium.webdriver.common.by import By


def js_click(
    driver: ChromiumDriver, by: By, value: str, force_scroll_end: Optional[bool] = False
):
    """
    Perform a "real click" with the mouse.
    Used primarily with sites that use awful Javascript type buttons that are fake buttons.
    Noting that the viewport must be focused on the element to click on.
    We use ActionChain to try and move to that portion of the page, but it is notoriously unreliable especially
    for pages with dynamic viewports.

    A common use case for this function is to click the submit button at the end of the page, so a utility to force
    jumping to the end of the page is provided using a hack. Activate by passing True in as the last param of the
    function.
    """
    js_button = driver.find_element(by, value)
    if force_scroll_end:
        # Yeah it's hacky, but so is this entire script
        for _ in range(0, 10):
            driver.find_element(By.CSS_SELECTOR, "body").send_keys(Keys.PAGE_DOWN)
        time.sleep(1)
    hover = ActionChains(driver).move_to_element(js_button)
    hover.perform()
    js_button.click()
